#!/usr/bin/env python3
#
# Iterate over a set of repositories in a json file and setup a shared directory containing them
#

import json
import os
import sys
import subprocess
import errno

import utils


parser = utils.ArgParser(description='Iterates over a set of repositories in a json file and sets up a shared directory containing them.')

parser.add_argument('repojson',
                    help="The json file containing the repositories to use")
parser.add_argument('sharedsrcdir',
                    help="The shared directory where the repos are to be transferred")
parser.add_argument('-p', '--publish-dir',
                    action='store',
                    help="Where to publish artefacts to (optional)")

args = parser.parse_args()

ourconfig = utils.loadconfig()

with open(args.repojson) as f:
    repos = json.load(f)

stashdir = utils.getconfig("REPO_STASH_DIR", ourconfig)

for repo in sorted(repos.keys()):
    utils.printheader("Intially fetching repo %s" % repo)
    utils.fetchgitrepo(args.sharedsrcdir, repo, repos[repo], stashdir)
    if args.publish_dir:
        utils.publishrepo(args.sharedsrcdir, repo, args.publish_dir)
